# OpenWall

A free self-hosted social wall using webdav as backend.

## Run with docker

```
docker run \
    -p 8080:8080 \
    -e CONFIG_FILE=/path/to/config.json \
    -e WEBDAV_URL=[url] \
    -e WEBDAV_USER=[user] \
    -e WEBDAV_PASSWORD=[pass] \
    -e LOG=[DEBUG|INFO|WARN|ERROR] \
    registry.gitlab.com/gfelbing/openwall:latest
```

where [url], [user], [pass] is replaced by your webdav credentials.

## Build from Source

### Requirements:

- yarn
- docker

### Compiling the Server/Client

```
./build.sh
```

### Building the Docker Image

```
docker build . -t [tag]
```

where [tag] is the docker image tag you want to use.

## Configure the Server

The Server can be configured with environment variables:

| Variable | Effect |
| -- | -------- |
| CONFIG_FILE | A path to a [config](server/src/Config.ts) object. |
| WEBDAV_URL | The url which contains the images. |
| WEBDAV_USER | The user to access the WEBDAV_URL |
| WEBDAV_PASSWORD | The password of the WEBDAV_USER |
| LOG | The log level. One of DEBUG, INFO, WARN, ERROR |

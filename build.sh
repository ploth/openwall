#! /usr/bin/env sh

BASEDIR=$(realpath $(dirname $0))

yarncmd() {
    PROJECT=$1
    COMMAND=$2
    YARN_BIN="$BASEDIR/node_modules/yarn/bin/yarn"

    echo "################## $PROJECT: $COMMAND ########################"

    cd "$BASEDIR/$PROJECT"
    $YARN_BIN install
    $YARN_BIN $COMMAND
}

cd "$BASEDIR"
yarn install

yarncmd server build
yarncmd client build


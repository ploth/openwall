FROM node:8-alpine
ADD server/dist /app
ADD server/package.json /app
ADD server/yarn.lock /app
ADD client/dist /app/static
WORKDIR /app
RUN yarn install
CMD node index.js
